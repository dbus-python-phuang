#!/usr/bin/env python
import dbus
import dbus.mainloop.glib
import dbus.connection
import gobject
import time

def main():
	loop = gobject.MainLoop ()
	conn = dbus.connection.Connection ("unix:abstract=/tmp/example-server")
	obj = conn.get_object ("no.need.name", "/org/freedesktop/ExampleObject")
	print obj.HelloWorld ("[%s] Hello World" % time.time())
	time.sleep (1)
	print obj.Exit ()

if __name__ == "__main__":
	dbus.mainloop.glib.DBusGMainLoop (set_as_default=True)
	main ()
