#!/usr/bin/env python
import dbus
import dbus.server
import dbus.lowlevel
import dbus.service
import dbus.mainloop.glib
import sys
import gobject

class ExampleObject (dbus.service.Object):
	SUPPORTS_MULTIPLE_CONNECTIONS = True

	@dbus.service.method (dbus_interface="org.freedesktop.ExampleObject", in_signature="s", out_signature="s")
	def HelloWorld (self, message):
		print message
		return "Reply : %s" % message

	@dbus.service.method (dbus_interface="org.freedesktop.ExampleObject", out_signature="s", async_callbacks=("reply_cb", "error_cb"))
	def Exit (self, reply_cb, error_cb):
		reply_cb ("Exit")
		sys.exit ()


class ExampleServer (dbus.server.Server):
	def __init__ (self):
		dbus.server.Server.__init__ (self, "unix:abstract=/tmp/example-server")
		self.register_object (ExampleObject (), "/org/freedesktop/ExampleObject")

	def new_connection (self, server, connection):
		connection.add_message_filter (self.message_filter_cb)
		print "New connection"

	def remove_connection (self, connection):
		print "Remove connection"

	def message_filter_cb (self, connection, message):
		if message.is_signal (dbus.LOCAL_IFACE, "Disconnected"):
			print "A connection was closed"
			return dbus.lowlevel.HANDLER_RESULT_NOT_YET_HANDLED

		print "Got a Message (%s) : " % message.__class__.__name__
		print "\t From:      %s" % message.get_sender ()
		print "\t To:        %s" % message.get_destination ()
		print "\t Interface: %s" % message.get_interface ()
		print "\t Path:      %s" % message.get_path ()
		print "\t Member:    %s" % message.get_member ()
		print "\t Arguments:"
		i = 0
		for arg in message.get_args_list():
			print "\t\t Arg[%d] : %s" % (i, arg)
			i = i + 1

		return dbus.lowlevel.HANDLER_RESULT_NOT_YET_HANDLED

if __name__ == "__main__":
	dbus.mainloop.glib.DBusGMainLoop (set_as_default = True)
	loop = gobject.MainLoop ()
	bus = ExampleServer ()
	print "ADDRESS=\"%s\"" % bus.get_address ()
	loop.run ()

